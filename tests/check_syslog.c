#ifdef __MINGW32__
#include <windows.h>
#endif

#include <syslog.h>
#include <stdio.h>

int main(void)
{
	init_syslog("");
	openlog ("syslog_w32", LOG_PID, LOG_DAEMON);
	printf("We're still on ?\n");
	int i;
	for (i=0;i<5; i++) syslog (LOG_NOTICE, " new syslog_w32 test, %d \n", i);
	printf("msgess done\n");	
	closelog();
        exit_syslog();
	printf("Done\n");
	return(0) ;
}

