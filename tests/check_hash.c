#ifdef __MINGW32__
#include <windows.h>
#endif

#include <check.h>
#include <openssl/evp.h>
#include <unistd.h>
#include <stdlib.h>


char To_Hash[10];
char Hash_Result[EVP_MAX_MD_SIZE*2 +1];
char Hash_Calc[EVP_MAX_MD_SIZE*2 +1];


void SHA256Hash(unsigned char digest[EVP_MAX_MD_SIZE], char *stringToHash);
void PrintSHA256Hash(unsigned char digest[EVP_MAX_MD_SIZE], char *Result);




void SHA256Hash(unsigned char digest[EVP_MAX_MD_SIZE], char *stringToHash)
{
 OpenSSL_add_all_digests(); 
 const EVP_MD *md = EVP_get_digestbyname("sha256");
 
 EVP_MD_CTX context;
 EVP_MD_CTX_init(&context);
 EVP_DigestInit_ex(&context, md, NULL); 
 EVP_DigestUpdate(&context, (unsigned char *)stringToHash, strlen(stringToHash));
 unsigned int digestSz;
 EVP_DigestFinal_ex(&context, digest, &digestSz);
 EVP_MD_CTX_cleanup(&context);
 EVP_cleanup();
}

void PrintSHA256Hash(unsigned char digest[EVP_MAX_MD_SIZE], char *buf)
{
#define DIGEST_SIZE_BYTES (256 / 8)
#define DIGEST_SIZE_HEX (DIGEST_SIZE_BYTES * 2)
 
 buf[0]=0;
 int i;
 for(i = 0; i < DIGEST_SIZE_BYTES; i++)
    {
    snprintf(&(buf[2*i]), 3, "%02x", digest[i]);
    }
}



void setup(void)
{

        sprintf(To_Hash,"12345678");
	sprintf(Hash_Result,"ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
        int i;
	for (i=0;i<(EVP_MAX_MD_SIZE*2 +1);i++)Hash_Calc[i]=0;
}

void teardown(void)
{
	;
}

START_TEST(check_hash)
{
	 unsigned char digest[EVP_MAX_MD_SIZE];
 	SHA256Hash(digest, To_Hash);
 	PrintSHA256Hash(digest,Hash_Calc );
	ck_assert_str_eq(Hash_Calc, Hash_Result);
}
END_TEST


Suite * check_suite(void)
{
	Suite *s;
	TCase *tc_static;
	
	s = suite_create("Hash_suite");
	
	/* Core test case */
	tc_static = tcase_create("Static");
	
	tcase_add_checked_fixture(tc_static, setup, teardown);
	
	tcase_add_test(tc_static, check_hash);
	suite_add_tcase(s, tc_static);
	
	return s;
}

int main(void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;
	
	s = check_suite();
	sr = srunner_create(s);
	
	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

