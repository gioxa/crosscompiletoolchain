#ifdef __MINGW32__
#include <windows.h>
#endif

#include <check.h>
#include <zlib.h>
#include <stdio.h>
#include <assert.h>

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

char IN_BUF[255];
char INTER_BUF[255];
char OUT_BUF[512];
char WANT_BUF[512];


int def(int * deflate_size)
{
    int ret;
    int insize=strlen(IN_BUF);
    z_stream strm;

    /* allocate deflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    ret = deflateInit(&strm, 1);

    if (ret != Z_OK)
        return ret;

    /* compress until end of file */
        strm.avail_in = insize;
        strm.next_in = IN_BUF;

        strm.avail_out = 512;
        strm.next_out = INTER_BUF;
        ret = deflate(&strm, Z_FINISH);    /* no bad return value */
        assert(ret != Z_STREAM_ERROR);  /* state not clobbered */

        * deflate_size = 512 - strm.avail_out;
        assert(strm.avail_in == 0);     /* all input will be used */

        if (strm.avail_in != 0)
        {
            //printf("deflate not complete, end\n");
            (void)deflateEnd(&strm);
            return Z_ERRNO;
        }


    /* clean up and return */
    (void)deflateEnd(&strm);
    return Z_OK;
}




void setup(void)
{

        sprintf(IN_BUF,"I want to deflate this properly if posible");
	sprintf(WANT_BUF,"7801ff54284fff2b5128ff5748494dff492c495528ffff2c562828ff2f482dffff54ff4c5328ff2fff4cff49050045580fff");
        int i;
	for (i=0;i<(255);i++)INTER_BUF[i]=0;
	for (i=0;i<(512);i++)OUT_BUF[i]=0;

}

void teardown(void)
{
	;
}

void PrintHEX(int size_def)
{
 int i;
 for(i = 0; i < size_def; i++)
    {
    snprintf(&(OUT_BUF[2*i]), 3, "%02x", INTER_BUF[i]);
    }
}

START_TEST(check_hash)
{
	int size_def=0;
	ck_assert_int_eq( def(&size_def),0);
        ck_assert_int_ne(size_def, 0);
 	PrintHEX(size_def);
	ck_assert_str_eq(OUT_BUF, WANT_BUF);
}
END_TEST


Suite * deflate_suite(void)
{
	Suite *s;
	TCase *tc_static;

	s = suite_create("delfate_suite");

	/* Core test case */
	tc_static = tcase_create("Static");

	tcase_add_checked_fixture(tc_static, setup, teardown);

	tcase_add_test(tc_static, check_hash);
	suite_add_tcase(s, tc_static);

	return s;
}

int main(void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = deflate_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
