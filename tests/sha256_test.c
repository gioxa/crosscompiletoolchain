#ifdef __MINGW32__
#include <windows.h>
#endif

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>
#include <stdlib.h>
#include <unistd.h>

int wflag = 0;

void SHA256Hash(unsigned char digest[EVP_MAX_MD_SIZE], char *stringToHash);
void PrintSHA256Hash(unsigned char digest[EVP_MAX_MD_SIZE], char *stringToHash);

const char * default_hash="12345678";

int main(int argc, char **argv)
{
 unsigned char digest[EVP_MAX_MD_SIZE];
 char * tohash=NULL;
  int c;

  opterr = 0;
  while ((c = getopt (argc, argv, "w")) != -1)
    switch (c)
      {
      case 'w':
        wflag = 1;
        break;
      }

   //printf("wflag =%d\n",wflag);

if (optind<argc)
{
  if ( argv[optind])
   tohash= argv[optind];
  else 
  {
   printf("big problem");
    exit(1);
  }
}
else
tohash=default_hash;

//printf("to Hash=%s\n",tohash);
 SHA256Hash(digest, tohash);
 PrintSHA256Hash(digest, tohash);
 return 0;
}

void SHA256Hash(unsigned char digest[EVP_MAX_MD_SIZE], char *stringToHash)
{
 OpenSSL_add_all_digests();
 
 const EVP_MD *md = EVP_get_digestbyname("sha256");
 
 EVP_MD_CTX context;
 EVP_MD_CTX_init(&context);
 EVP_DigestInit_ex(&context, md, NULL); 
 EVP_DigestUpdate(&context, (unsigned char *)stringToHash, strlen(stringToHash));
 
 unsigned int digestSz;
 EVP_DigestFinal_ex(&context, digest, &digestSz);
 EVP_MD_CTX_cleanup(&context);
 
 EVP_cleanup();
}

void PrintSHA256Hash(unsigned char digest[EVP_MAX_MD_SIZE], char *stringToHash)
{
#define DIGEST_SIZE_BYTES (256 / 8)
#define DIGEST_SIZE_HEX (DIGEST_SIZE_BYTES * 2)

 char buf[DIGEST_SIZE_HEX + 1] = {0};

 int i;
 for(i = 0; i < DIGEST_SIZE_BYTES; i++)
    {
    snprintf(&(buf[2*i]), 3, "%02x", digest[i]);
    }

#ifdef __MINGW32__
if (wflag==1) MessageBox(NULL, buf, "Digest", MB_OK);
else 
printf("%s\r\n",buf);

#else 
 printf("%s\r\n",buf);

#endif

}

