#ifdef __MINGW32__
#include <windows.h>
#endif

#include <check.h>
#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

int a;

void setup(void)
{
	a=5;
}

void teardown(void)
{
	a=0;
}

START_TEST(check_check)
{
	ck_assert_int_eq(a, 5);
}
END_TEST


Suite * check_suite(void)
{
	Suite *s;
	TCase *tc_static;

	s = suite_create("check_suite");

	/* Core test case */
	tc_static = tcase_create("Static");

	tcase_add_checked_fixture(tc_static, setup, teardown);

	tcase_add_test(tc_static, check_check);
	suite_add_tcase(s, tc_static);

	return s;
}

int main(void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = check_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_VERBOSE);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? 0 : 1;
}
