# cros-compile images from rpm distro


## Purpose

Creating dedicated docker images for cross-compiling software for multiple architectures

## Usage of image

autoconfig example, and install in the toolchain @ `$PREFIX_custom`

```yaml
.compile: &compile
  image: gioxa/base-tc-${CI_JOB_NAME#compile-}
  before_script:
  - |
     export JOB=${CI_JOB_NAME#compile-}
     . bootstrap
  script:
  - CC=$TARGET-gcc CXX=$TARGET-g++ ../configure --host=$TARGET --prefix=$PREFIX_custom
  - make
  - make install
compile-el7-x64: *compile
compile-el7-arm64: *compile
compile-el7-i386: *compile
```

more examples in `extra_libs.sh`.

## Build Concept

we start by building a host system with `Development Tools`, and in this case a few libs used for creating source code for the project.

secondly for each requested architecture, we build a rootfs with from the rpm distro and extract the glibc in /usr/{includes,lib}, next we compile the linker and compiler.

![Centos Inside](https://gitlab.gioxa.com/uploads/project/avatar/167/images-4.png )
