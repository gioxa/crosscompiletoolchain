#!/usr/bin/bash
#
#  extra_libs.sh
#
# Created by Danny Goossen on 28/12/2017.
#
# MIT License
#
# Copyright (c) 2017 oc-runner, Gioxa Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

. gcc_options/parallel_make

mkdir -pv $CI_PROJECT_DIR/extra_packages/Build

function make_pthreads_win()
{
  . trap_print
  cd $CI_PROJECT_DIR/extra_packages/Build

  tar -xf ../pthreads-w32-2-9-1-release.tar.gz
  cd pthreads-w32-2-9-1-release/
  make clean > /dev/null

  mkdir -pv $PREFIX_custom/lib/pkgconfig

cat << _EOF > $PREFIX_custom/lib/pkgconfig/pthreadGC2.pc
prefix=$PREFIX_custom
exec_prefix=\${prefix}
libdir=\${exec_prefix}/lib
includedir=\${prefix}/include

Name: pthreadGC2
Description: Library to interface with win32 threads model
Requires:
Version: 2.9.1
Libs: -L\${libdir} -lpthreadGC2
Cflags: -I\${includedir}
_EOF

ln -sfv $PREFIX_custom/lib/pkgconfig/pthreadGC2.pc ./pthread.pc

  cp -fv pthread.h  sched.h semaphore.h $PREFIX_custom/include/

  make -j8 CROSS=$TARGET- GC-inlined > /dev/null
  make -j8 CROSS=$TARGET- GC-static > /dev/null
  cp -fv libpthreadGC2.a $PREFIX_custom/lib/
  cp -fv pthreadGC2.dll $PREFIX_custom/bin/
  #ln -sfv $S_ROOT/lib/libpthreadGC2.a ./libpthread.a
  #ln -svf $S_ROOT/lib/pthreadGC2.dll ./pthread.dll
  make clean > /dev/null
  trap - DEBUG
  cd $CI_PROJECT_DIR
}

function make_check()
{
  . trap_print

  cd $CI_PROJECT_DIR/extra_packages/Build
  tar -xf ../check-0.10.0.tar.gz

  #patch , from `diff -Naur check-0.10.0 check-0.10.0.b  > patch_check_android.txt`
  patch -p0 < ../check_foreign.patch

  cd check-0.10.0

  mkdir -pv Build
  cd Build
  autoreconf -ifv ../
  if [ -z "${RELEASE%darwin*}" ]; then
    CC=$TARGET-clang CXX=$TARGET-clang++ ../configure --host=$TARGET --prefix=$PREFIX_custom
  else
    CC=$TARGET-gcc CXX=$TARGET-g++ ../configure --host=$TARGET --prefix=$PREFIX_custom
  fi
  make $PARALLEL_MAKE > /dev/null
  make install > /dev/null
  make distclean > /dev/null
  trap - DEBUG
  cd $CI_PROJECT_DIR
}

function make_syslog_win()
{
  . trap_print
  cd $CI_PROJECT_DIR/extra_packages/Build
  tar -xf ../syslog-win32-1.0.0.tar.gz
  cd syslog-win32-1.0.0
  PREFIX=$PREFIX_custom GCC_PREFIX=$TARGET ./make_dll > /dev/null
  cd $CI_PROJECT_DIR
  trap - DEBUG
}

function make_openssl()
{
. trap_print
cd $CI_PROJECT_DIR/extra_packages/Build

tar -xf ../openssl-1.0.2h.tar.gz
cd openssl-1.0.2h

if [ -z "${RELEASE%darwin*}" ]; then
  echo "==>darwin ssl"
  rm -rf $PREFIX_custom/include/openssl
  rm -rf $PREFIX_custom/include/CommonCrypto
  rm -rf $PREFIX_custom/lib/libcrypto.*
  rm -rf $PREFIX_custom/lib/libssl.*
  [ "$FAKE_ARCH" = "x86_64" ] && CONFIGURE_TARGET=darwin64-x86_64-cc
  [ "$FAKE_ARCH" = "i386" ] && CONFIGURE_TARGET=darwin-i386-cc
  #export CC=$TARGET-cc;
  #export CXX=$TARGET-clang++; export LD=$TARGET-ld; export LIBTOOL=$TARGET-libtool
  #export AS=$TARGET-as; export AR=$TARGET-ar; export RANLIB=$TARGET-ranlib
  #/usr/bin/sed -i 's,.*target already defined.*,$target=$_;,g' Configure
  sed -i 's/CC= cc/CC= $TARGET-cc/g' Makefile.org
  sed -i 's/MAKEDEPPROG=makedepend/MAKEDEPPROG=$(CC) -M/g' Makefile.org
else
  if [ -z "${RELEASE%mingw32}" ]; then
    echo "==>mingw SSL"
    [ "$FAKE_ARCH" = "x86_64" ] && CONFIGURE_TARGET=mingw64
    [ "$FAKE_ARCH" = "i686" ] && CONFIGURE_TARGET=mingw
       /usr/bin/sed -i 's/:.dll.a/ -Wl,--export-all -shared:.dll.a/g' Configure
       #/usr/bin/sed -i 's,.*target already defined.*,$target=$_;,g' Configure
       sed -i 's/CC= cc/CC= $TARGET-cc/g' Makefile.org
  else
    echo "we should not be here"
    exit 1
  fi
fi
./Configure $CONFIGURE_TARGET shared --cross-compile-prefix=$TARGET- --prefix=$PREFIX_custom
make depend > /dev/null
sed -i '/#define HEADER_X509V3_H/a \\n#ifdef X509_NAME\n#undef X509_NAME\n#endif' include/openssl/x509v3.h
make > /dev/null
make install > /dev/null
make clean > /dev/null
cd $CI_PROJECT_DIR
trap - DEBUG
}

make_zlib()
{
  . trap_print
  cd $CI_PROJECT_DIR/extra_packages/Build
  tar -xf ../zlib-1.2.8.tar.gz
  patch zlib-1.2.8/Makefile.in -i ../patch_zlib.txt
  cd zlib-1.2.8

export CC=$TARGET-gcc
export CXX=$TARGET-g++
export CPP=$TARGET-cpp
export RANLIB=$TARGET-ranlib

./configure --prefix=$PREFIX_custom
make >/dev/null
make install
make clean > /dev/null
trap - DEBUG
cd $CI_PROJECT_DIR
unset CC
unset CXX
unset CPP
unset RANLIB
}



cd $CI_PROJECT_DIR
. trap_print
#### mingw need pthreads, let's do before CHECK
if [ -z "${RELEASE%mingw*}" ]; then
  echo -e "\n\033[36m-------- Install pthreads_win ------\033[0;m"
  make_pthreads_win
  echo -e "\n\033[36m-------- Install zlib ------\033[0;m"
  make_zlib
fi

## all targets need CHECK
echo -e "\n\033[36m-------- Install CHECK $TARGET ------\033[0;m"
make_check

## all but Enterprise Linux need openssl
if [ ! -z "${RELEASE%el*}" ]; then
  echo -e "\n\033[36m-------- Install openssl $TARGET ------\033[0;m"
make_openssl
fi

## mingw need some logging
if [ -z "${RELEASE%mingw*}" ]; then
  echo -e "\n\033[36m-------- Install syslog-win ------\033[0;m"
  make_syslog_win
fi
trap - DEBUG
cd $CI_PROJECT_DIR
