#!/usr/bin/bash
#
#  make_rootfs
#
# Created by Danny Goossen on 28/12/2017.
#
# MIT License
#
# Copyright (c) 2017 oc-runner, Gioxa Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

  echo -e "\033[36;1m ++++++++++++++++++++++++++++++++++++\n"
  echo -e "    create yum cache\n"
  echo -e " ++++++++++++++++++++++++++++++++++++\n\033[0;m"

  if [ -z "$BASE" ]; then
    BASE=${PWD}
  fi
  if [ -z "$target" ]; then
    target=${PWD}/rootfs
    echo -e "\nUsing default target: \'./rootfs\'\n"
  fi
  if [ -d "$target" ]; then
    echo -e "\n\033[31m there is no target defined!!\033[0;m\n"
    echo -e "\n\033[31;1m --- FAILED ---\033[0;m\n"
    f_fail
  fi
  rm -fr $target/*

  mkdir -pv $BASE/RPMS
  echo "FAKE_ARCH=$FAKE_ARCH"
    if [ "$target_release" != "6" ] && [ "$FAKE_ARCH" != "x86_64" ];then
    yum_config=yum_target_altarch.conf
  else
    yum_config=yum_target.conf
  fi
  OS_CONFIG=make_target.conf
 echo "yum config"
  cat $yum_config
  echo

  [ ! -f "$OS_CONFIG" ] && echo "need an make_os.conf in current dir, abort!" && exit 1

  . $OS_CONFIG

  if [ ! -d "/usr/sbin/glibc-fake" ]; then
  echo "missing GLIBC_FAKE"
  echo -e "\n\033[31;1m --- FAILED ---\033[0;m\n"
  exit 1
  fi
  export GLIBC_FAKE=/usr/sbin/glibc-fake

  echo "Building Centos rootfs"
  if [ -z "$target_release" ]; then
      echo "using centos 7 release"
      target_release=7
  fi
  echo
  echo "Target relase : $target_release"
  echo
  if [ -z "$yum_config" ]; then
      echo "you need to provide the yum_config in make_os.conf, abort!"
      exit 1
  fi

  echo -e "\033[33m-------- RPM prepare user db ------\033[0;m"
  mkdir -pv $target/var/lib/rpm
  fakeroot rpmdb --initdb --dbpath $target/var/lib/rpm

  echo "%_dbpath $target/var/lib/rpm" | cat > $HOME/.rpmmacros
  echo "%_tmppath /tmp" | cat >> $HOME/.rpmmacros
#  echo "%_arch armv7hl" | cat >> $HOME/.rpmmacros

  echo -e "\033[33m-------- verify pub keys ------\033[0;m\n"
  gpg --quiet --with-fingerprint $BASE/GPG-KEYS/*
  echo -e "\033[33m-------- RPM import pub keys ------\033[0;m\n"
  fakeroot rpm --verbose --import $BASE/GPG-KEYS/*
  rpm -q gpg-pubkey --qf '%{NAME}-%{VERSION}-%{RELEASE}\t%{SUMMARY}\n'

# TODO figure out how to do different archs
#
  echo -e "\n\033[33m-------- YUM download packages ------\033[0;m\n"
  #[ -e "$target/yumcache" ] && rm -rf "$target/yumcache"

  mkdir -pv "$target/var/cache"


  [ -e "$BASE/../rpmcache/yum" ] && cp -rf "$BASE/../rpmcache/yum" "$target/var/cache/" || mkdir -pv "$target/var/cache/yum"
  chmod -R 0755 "$target/var/cache/yum"
  #fakeroot tree $target/var/cache/yum/
  #fakeroot yum -c "$yum_config" --installroot="$target" --releasever="$target_release" makecache
  #--rpmverbosity=debug -v -d3
  if [ -n "$install_groups" ];then
     #set -x
     echo "Install groups : $install_groups"
     set +e
     LD_PRELOAD=fake_arch.so /usr/bin/fakeroot yum -c "$yum_config" --downloadonly --nogpgcheck --installroot="$target" --releasever="$target_release" -y groupinstall $install_groups
     EXITCODE=$?
     #set +x
     set -e
     if [ "$EXITCODE" != "0" ]; then
       echo -e "\n\033[31;1m --- FAILED ---\033[0;m\n"
       exit $EXITCODE
     fi
  fi
  #--downloaddir=$BASE/packages
  if [ -n "$install_packages" ]; then
    #set -x
    echo "Install groups : $install_groups"
    set +e
    LD_PRELOAD=fake_arch.so /usr/bin/fakeroot yum -c "$yum_config" --downloadonly --installroot="$target" --releasever=$target_release -y install $install_packages
    EXITCODE=$?
    set -e
    if [ "$EXITCODE" != "0" ]; then
      echo -e "\n\033[31;1m --- FAILED ---\033[0;m\n"
      exit $EXITCODE
    fi
  fi
  #/usr/bin/fakeroot mkdir -p $BASE/cache/yum
  #/usr/bin/fakeroot cp -rf "$target/var/cache/yum/." "$BASE/cache/yum/"
  mkdir -pv $BASE/../rpmcache
  cp -rf $target/var/cache/yum $BASE/../rpmcache/
  rm -rf $target


    exit_code=$?
    if [[ "$exit_code" == "0" ]]; then
        echo -e "\n\033[32;1m *** Success ***\033[0;m\n"
    else
        echo -e "\n\033[31;1m --- FAILED ---\033[0;m\n"
    fi
